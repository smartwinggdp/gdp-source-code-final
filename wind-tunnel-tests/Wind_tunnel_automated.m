%% Serial Setup
% Set comPort to test rig Arduino Port Number
%
% Set ServocomPort to Servo Arduino Port Number
comPort = '/dev/tty.usbmodem1421'; 
ServocomPort = '/dev/tty.usbmodem1411'; 
[Arduino, sflag] = setupSerial(comPort);
[ServoArd, serflag] = setupSerial(ServocomPort);

%% Load Calibration Results
C_load = load('calib_matrix.mat');
C = C_load.C_struct.CW1{3};

%% Testing Parameters

% Number of samples to be recorded per condition
samples = 15;

% Angle of attack
AoA_arr = 0:2:14;
AoA_arr = [AoA_arr -AoA_arr];

% Flap angle 
flap_arr = 0:3:30;
flap_arr = [flap_arr -flap_arr];

% Size
AoAs = size(AoA_arr,2);
flap_angles = size(flap_arr,2);

%% Begin request for data
fprintf(Arduino,'%c', 'c');

%% Forces measured in the absence of wind
R_no_wind = zeros(15,6);
Force = zeros(size(R_no_wind,1),5);
R_no_wind(:,1) = 1; 

%get data for zero flap and angle of attack
fprintf(ServoArd,'%s','f000');
pause(2);
fprintf(ServoArd,'%s','w000');

for i = 1:1:size(R_no_wind,1)
    fprintf(Arduino,'%c', 'r');
    R_no_wind(i,2) = readSerial(Arduino,'%d');
    R_no_wind(i,3) = readSerial(Arduino,'%d');
    R_no_wind(i,4) = readSerial(Arduino,'%d');
    R_no_wind(i,5) = readSerial(Arduino,'%d');
    R_no_wind(i,6) = readSerial(Arduino,'%d');
    Force(i,1:3) = R_no_wind(i,:) * C;
end

% Convert Units
Force(:,1:2) = Force(:,1:2) * 0.001 * 9.81;
Force(:,3) = Force(:,3) * 0.001; 

%% Begin Wind tunnel test

% Press 'enter' to proceed
startin = input('Start?');

R = zeros(samples*flap_angles*AoAs,6);
R(:,1) = 1;
Force_mat =  zeros(samples*flap_angles*AoAs, 5);
% Record flap angles and AoAs combination
for aoa_i = 1:1:AoAs
    for fa_i = 1:1:flap_angles
        for smp = 1:1:samples
            Force_mat((aoa_i-1)*samples*flap_angles + (fa_i-1)*samples + smp,4) = AoA_arr(aoa_i);
            Force_mat((aoa_i-1)*samples*flap_angles + (fa_i-1)*samples + smp,5) = flap_arr(fa_i);
        end
    end
end

% Acquire data for each flap angle, Aoa combination
for aoa_i = 1:1:AoAs
    %send command to servo arduino : w<angle>
   fprintf(ServoArd,'w%3s',int2str(AoA_arr(aoa_i)));
    
    for fa_i = 1:1:flap_angles
        %send command to servo arduino : f<angle>
        fprintf(ServoArd,'f%3s',int2str(flap_arr(fa_i)));
        %tell arduino to wait
        fprintf(Arduino,'%c','d');
        for smp =  1:1:samples
            % Request data for sample
            fprintf(Arduino,'%c', 'r');
            current_it = (aoa_i-1)*samples*flap_angles + (fa_i-1)*samples + smp;
            R(current_it,2) = readSerial(Arduino,'%d');
            R(current_it,3) = readSerial(Arduino,'%d');
            R(current_it,4) = readSerial(Arduino,'%d');
            R(current_it,5) = readSerial(Arduino,'%d');
            R(current_it,6) = readSerial(Arduino,'%d');
            Force_mat(current_it,1:3) = R(current_it,:) * C;
            disp(sprintf('LIFT = %.6f g, DRAG = %.6f g, PM = %.6f gm, Aoa = %d, Flap angle = %d',  Force_mat(current_it,:)));
            %received all data for this sample > request new sample
        end
    end
end

% Convert Units
Force_mat(:,1:2) = Force_mat(:,1:2) * 0.001 * 9.81;
Force_mat(:,3) = Force_mat(:,3) * 0.001; 

%% Save data and Cleanup

% Tell Test Rig Arduino to Stop 
fprintf(Arduino,'%c', 'e');

% Reset Wing section to 0, 0
fprintf(ServoArd,'%s','f000');
pause(4);
fprintf(ServoArd,'%s','w000');

save('Wind_tunnel_lc_data.mat','R','R_no_wind');
save('Wind_tunnel_force_data.mat','Force_mat','F_no_wind');

fclose(ServoArd);
delete(ServoArd);
fclose(Arduino);
delete(Arduino);
