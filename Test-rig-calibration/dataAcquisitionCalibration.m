clc 
clear

%% Establish Serial Communication with Arduino
% Run this script to get data from load cells via Arduino

% Set comPort to whichever port Arduino is connected to
comPort = '/dev/tty.usbmodem1421';
[Arduino, flag] = setupSerial(comPort);

%% Initialise Parameters

% Loading Conditions
calibrationPerms
L_loc = Lift_location;
Y = Test_Cases;
N = size(Y, 1);

%unit row and 6 columns, one for each tranducer
R = zeros(N, 6);
%set unit row
R(:, 1) = 1;
offsets = zeros(1, 5);

%find offsets for the data collected
disp('getting offsets for transducers');
offsets(1,1) = readSerial(Arduino,'%d');
offsets(1,2) = readSerial(Arduino,'%d');
offsets(1,3) = readSerial(Arduino,'%d');
offsets(1,4) = readSerial(Arduino,'%d');
offsets(1,5) = readSerial(Arduino,'%d');
 %indicate to arduino that data has been received
fprintf(Arduino,'%c', 'c');

%% Estimate R matrix
for p =  1:1:N
    %display which loading condition
    disp(sprintf('Loading condition %d ; Lift = %f, Drag = %f , pitching moment = %f, Lift_location = %f cm', p , Y(p,:), L_loc(p)));
    R(p,2) = readSerial(Arduino,'%d');
    R(p,3) = readSerial(Arduino,'%d');
    R(p,4) = readSerial(Arduino,'%d');
    R(p,5) = readSerial(Arduino,'%d');
    R(p,6) = readSerial(Arduino,'%d');
    if (p == N) %indicate to arduino that last data has been sent
        fprintf(Arduino,'%c', 'e'); 
    else %indicate to arduino that data has been received
        fprintf(Arduino,'%c', 'g'); 
    end
end

%% Save Data and Cleanup
save('calib_data100317_3.mat','R','Y','offsets');

fclose(Arduino);
delete(Arduino);
