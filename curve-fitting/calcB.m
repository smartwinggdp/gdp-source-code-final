function [b] = calcB(n,u,v,w,q)
%% Inputs
% n: number of samples
%
% u: vector representing the main diagonal
%
% v: vector representing the second diagonal symmetric about u
%
% w: vector representing the third diagonal symmetric about u
%
% q: y values (RHS of equation).
%% LDL factorization of diagonals
v(2) = v(2)/u(2);
w(2) = w(2)/u(2);

for j = 3:1:n-1
    u(j) = u(j) - (u(j-2) * w(j-2) * w(j-2)) - (u(j-1) * v(j-1) * v(j-1));
	v(j) = (v(j) - (u(j-1) * v(j-1) * w(j-1)))/u(j);
	w(j) = w(j)/u(j);    
end
u(n) = u(n) - (u(n-2) * w(n-2) * w(n-2)) - (u(n-1) * v(n-1) * v(n-1));


%% Forward substitution
%relevant values between 2 to n. position 1 is padding
for j = 3:1:n
    q(j) = q(j) - (v(j-1) * q(j-1)) - (w(j-2)*q(j-2));
end
for j = 2:1:n
    q(j) = q(j)/u(j);
end

%% Back substitution
% At n row :  b(n) = q(n)
%
% Position 1 is padding. 
%
% hence b is asssinged from n to 2. b(size) and b(1) = 0
for j = n-1:-1:2
    q(j) = q(j) - (v(j) * q(j+1)) - (w(j)*q(j+2));
end
b = q;
end
